describe('Technical Test', () => {
  it('Google Form', () => {
    cy.visit('https://forms.office.com/pages/responsepage.aspx?id=is2XW8LLaEmfFhLKD9VwE9lpKmxdveNGmMWKETZvAWNUMzhBV1lYTlc1SDNRS00xRVg4OFhPODlQTS4u');
    //quetion1
    cy.get('[aria-describedby="r9f62b2a1fbe746ab953326f6937d4e73_validationError"]').type("Rian Purnomo");
    //quetion2
    cy.get('[aria-describedby="r5c2dd5cf6732459894e3d1cb504c8110_validationError"]').type("085226248478");
    //quetion3
    cy.get('[data-automation-value="Affordable"]').click();
    //quetion4
    cy.get('[aria-label="5 Star"]').click()
    //qution5
    cy.get('[id="DatePicker0-label"]').click();
    cy.get('[id="DatePicker0-label"]').type('2/21/2024');
    //submit form
    cy.get('[data-automation-id="submitButton"]').click();
    cy.wait(2000);
    //validate contain if successfully
    cy.contains('Your response was submitted.').should('exist');
    // cy.contains('Your response has been successfully recorded.').should('exist');
  })
})
